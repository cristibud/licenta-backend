package findgenre.business.service;

import findgenre.business.dto.UserDTO;
import findgenre.business.dto.UserDTOTransform;
import findgenre.business.dto.UserRegisterDTO;
import findgenre.business.exception.BusinessException;
import findgenre.business.exception.ExceptionCode;
import findgenre.business.utils.Encryptor;
import findgenre.persistence.entity.User;
import findgenre.persistence.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
    private UserDTOTransform userDTOTransform;

    public UserRegisterDTO register(UserRegisterDTO userRegisterDTO) throws BusinessException {
        validForRegister(userRegisterDTO);
        User user = userDTOTransform.toRegisterEntity(userRegisterDTO);
        user.setPassword(Encryptor.encrypt(userRegisterDTO.getPassword()));
        userRepository.save(user);
        return userDTOTransform.fromRegisterEntity(user);
    }

    public UserDTO editUser(UserDTO user) throws BusinessException {
        Optional<User> userFind = userRepository.findById(user.getId());
        if (userFind.isPresent()) {
            User oldUser = userFind.get();
            oldUser.setName(user.getName());
            oldUser.setEmail(user.getEmail());
            oldUser.setPassword(user.getPassword());
            User userSaved = userRepository.save(oldUser);
            return userDTOTransform.fromEntity(userSaved);
        } else {
            throw new BusinessException(ExceptionCode.USER_NOT_EXIST);
        }
    }

    public UserDTO login(String email, String password) throws BusinessException {
        Optional<User> usersByEmail = userRepository.findByEmail(email);
        if (!usersByEmail.isPresent()) {
            throw new BusinessException(ExceptionCode.EMAIL_NOT_VALID);

        }

        if (!password.equals(Encryptor.decrypt(usersByEmail.get().getPassword()))) {
            throw new BusinessException(ExceptionCode.PASSWORD_NOT_VALID);
        }

        User user = usersByEmail.get();
        System.out.println(user.getPassword());
        return userDTOTransform.fromEntity(user);
    }

    public List<UserDTO> getAllUser() {
        List<UserDTO> users = new ArrayList<UserDTO>();
        userRepository.findAll().forEach(user -> {
            users.add(userDTOTransform.fromEntity(user));
        });
        return users;
    }

    private boolean isValidEmail(String email) {
        final Pattern validEmailAddressRegex =
                Pattern.compile("^[A-Z0-9._%+-]+@gmail.com$", Pattern.CASE_INSENSITIVE);
        Matcher matcher = validEmailAddressRegex.matcher(email);
        return matcher.find();
    }

    private boolean validateFields(UserRegisterDTO user) {

        return  user.getPassword() != null
                && isValidEmail(user.getEmail()) && user.getConfirmationPassword() == user.getPassword();
    }

    private void validForRegister(UserRegisterDTO user) throws BusinessException {
        if (userRepository.findByEmail(user.getEmail()).isPresent()) {
            throw new BusinessException(ExceptionCode.EMAIL_EXIST_ALREADY);
        }

        if (!validateFields(user)) {
            throw new BusinessException(ExceptionCode.USER_VALIDATION_EXCEPTION);
        }
    }
}
