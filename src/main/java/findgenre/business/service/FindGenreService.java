package findgenre.business.service;

import java.io.*;

public class FindGenreService {
    public String runScript(String id) throws IOException {
        //Create the process to find the genre of the song
        String cLine = "py E:\\Licenta\\Music-Genre-Classification\\findgenre.py " + id;
        Process process;
        process = Runtime.getRuntime().exec(cLine);
        try{
            String line, finalLine = "";
            BufferedReader input =
                    new BufferedReader
                            (new InputStreamReader(process.getInputStream()));
            while ((line = input.readLine()) != null) {
                finalLine = line;
            }
            input.close();
            return finalLine;
        }catch(Exception e) {
            System.out.println("Exception: " + e.toString());
        }
        return "Unknown";
    }
}