package findgenre.business.service;

import findgenre.business.dto.SongDTO;
import findgenre.business.dto.SongDTOTransform;
import findgenre.business.exception.BusinessException;
import findgenre.business.exception.ExceptionCode;
import findgenre.persistence.entity.Song;
import findgenre.persistence.entity.SongUser;
import findgenre.persistence.entity.User;
import findgenre.persistence.repository.SongRepository;
import findgenre.persistence.repository.SongUserRepository;
import findgenre.persistence.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class SongService {

    private String youtubeAPIURL = "https://www.googleapis.com/youtube/v3/videos/?part";

    @Autowired
    private SongRepository songRepository;
    @Autowired
    private SongUserRepository songUserRepository;

    @Autowired
    private UserRepository userRepository;
    private SongDTOTransform songDTOTransform;


    public List<SongDTO> getAllSong(String email) throws BusinessException {
        Optional<User> user = userRepository.findByEmail(email);
        List<SongDTO> songDTOS = new ArrayList<SongDTO>();
        List<Song> songs = songRepository.findAllByUserId(user.get().getId());
        songs.forEach(account -> {
            songDTOS.add(songDTOTransform.fromEntity(account));
        });

        if (songDTOS.isEmpty()) {
            throw new BusinessException(ExceptionCode.SONG_NOT_EXIST);
        }
        return songDTOS;
    }

    public SongDTO getSong(String id) throws BusinessException {
        Optional<Song> songFind = songRepository.findById(id);
        if (songFind.isPresent()) {
            Song song = songFind.get();
            return songDTOTransform.fromEntity(song);
        } else {
            throw new BusinessException(ExceptionCode.SONG_NOT_EXIST);
        }
    }

    public SongDTO updateSongGenre(SongDTO songDTO) throws BusinessException {
        Optional<Song> songFind = songRepository.findById(songDTO.getSongId());
        System.out.println(songDTO.getSongId());
        if (songFind.isPresent()) {
            Song song = songFind.get();
            song.setGenre(songDTO.getGenre());
            Song songSaved = songRepository.save(song);
            return songDTOTransform.fromEntity(songSaved);
        } else {
            throw new BusinessException(ExceptionCode.SONG_NOT_EXIST);
        }

    }

    public SongDTO insertSong(SongDTO songDTO, int id) throws BusinessException{
        if(songDTO == null){
            throw new BusinessException(ExceptionCode.SONG_IS_NULL);
        } else {
            Song song = songDTOTransform.toEntity(songDTO);
            songRepository.save(song);
            SongUser songUser = new SongUser(2, song.getId(),id);
            songUserRepository.save(songUser);
            return songDTOTransform.fromEntity(song);
        }
    }

    public SongDTO delete(int id) throws BusinessException {
        if((Integer) id == null) {
            throw new BusinessException(ExceptionCode.SONG_IS_NULL);
        }

        Optional<Song> song = songRepository.findById(id);
        if(!song.isPresent()){
            throw new BusinessException(ExceptionCode.SONG_NOT_EXIST);
        }else{
            songRepository.delete(song.get());
            SongDTO songDTO = songDTOTransform.fromEntity(song.get());
            return  songDTO;
        }
    }



}
