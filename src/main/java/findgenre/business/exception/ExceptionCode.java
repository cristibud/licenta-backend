package findgenre.business.exception;

public enum ExceptionCode {

    UNKNOWN_EXCEPTION(1000, "Unknown"),
    USER_VALIDATION_EXCEPTION(1001, "Validation Exception"),
    SONG_NOT_EXIST(1204, "Song not exist."),
    SONG_IS_NULL(1200, "Song value is null."),
    SONG_EXIST_ALREADY(1201, "Song already exists."),
    ENCRYPT_FAIL(1002, "Password encryption failed."),
    PASSWORD_NOT_VALID(1003, "Password not valid."),
    EMAIL_NOT_VALID(1004, "Email not valid."),
    USER_NOT_EXIST(1011, "The user does not exist"),
    EMAIL_EXIST_ALREADY(1001, "Email already exists.");

    int id;
    String message;

    ExceptionCode(int id, String message) {
        this.id = id;
        this.message = message;
    }

    public int getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }

}
