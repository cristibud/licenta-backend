package findgenre.business.controller;


import findgenre.business.dto.SongDTO;
import findgenre.business.dto.SongDTOTransform;
import findgenre.business.exception.BusinessException;
import findgenre.business.service.FindGenreService;
import findgenre.business.service.SongService;
import findgenre.persistence.entity.Song;
import findgenre.persistence.entity.User;
import findgenre.persistence.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/song")
public class SongController {

    @Autowired
    SongService songService;

    @Autowired
    UserRepository userRepository;

    FindGenreService findGenreService = new FindGenreService();

    private SongDTOTransform songDTOTransform;

    @GetMapping("/allSongs")
    public List<SongDTO> getAllSongs(@RequestParam("email") String email) throws BusinessException {
        return songService.getAllSong(email);
    }

    @GetMapping("/getSong")
    public SongDTO getSong(@RequestParam("id") String id) throws BusinessException {
        return songService.getSong(id);
    }

    @PutMapping("/updateSong")
    @RequestMapping(method = RequestMethod.PUT, value = "/updateSong")
    public ResponseEntity<?> updateSong(@RequestBody SongDTO songDTO) {
        try {
            SongDTO songDTO1 = songService.updateSongGenre(songDTO);
            return new ResponseEntity<>(songDTO1, HttpStatus.OK);
        } catch (BusinessException e) {
            return new ResponseEntity<>(e.getExceptionCode(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/addSong")
    public ResponseEntity<?> insert(@RequestBody SongDTO songDTO, @RequestParam String email){
        try{
            Song song = songDTOTransform.toEntity(songDTO);
            Optional <User> user = userRepository.findByEmail(email);
            if(user.isPresent()){
                User userSaved = user.get();
                SongDTO newSongDTO = songDTOTransform.fromEntity(song);
                try{
                    newSongDTO.setGenre(findGenreService.runScript(song.getSongId()));
                } catch (Exception e) {
                    e.getMessage();
                }
                SongDTO songDTO1 = songService.insertSong(newSongDTO, userSaved.getId());
                return  new ResponseEntity<>(songDTO1, HttpStatus.OK);
            } else {
                return null;
            }
        } catch (BusinessException b){
            return new ResponseEntity<>(b.getExceptionCode(),HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping(value = "/delete")
    public ResponseEntity<?> deleteSong(@RequestParam("id") int id){
        try{
            SongDTO songDTO = songService.delete(id);
            return new ResponseEntity<>(songDTO,HttpStatus.OK);
        }catch (BusinessException b){
            return new ResponseEntity<>(b, HttpStatus.BAD_REQUEST);
        }
    }
}
