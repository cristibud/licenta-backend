package findgenre.business.controller;

import findgenre.business.dto.UserDTO;
import findgenre.business.dto.UserRegisterDTO;
import findgenre.business.exception.BusinessException;
import findgenre.business.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/home")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/users")
    public List<UserDTO> getAllUsers(){
        return  userService.getAllUser();
    }

    @PutMapping("/editUser")
    public ResponseEntity<?> editUser(@RequestBody UserDTO userDTO) {
        try {
            UserDTO userDTO1 = userService.editUser(userDTO);
            return new ResponseEntity<UserDTO>(userDTO1,HttpStatus.OK);
        } catch (BusinessException e) {
            return new ResponseEntity<>(e.getExceptionCode(),HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/register")
    public ResponseEntity<?> register(@RequestBody UserRegisterDTO userDTO){
        try{
            UserRegisterDTO userRegisteredDTO = userService.register(userDTO);
            return new ResponseEntity<>(userRegisteredDTO,HttpStatus.OK);
        }catch(BusinessException b){
            return new ResponseEntity<>(b.getExceptionCode(),HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/login")
    public ResponseEntity<?> login (@RequestParam("email") String email, @RequestParam("password") String password){
        try{
            UserDTO userDTO = userService.login(email,password);
            return new ResponseEntity<>(userDTO,HttpStatus.OK);
        }catch(BusinessException b){
            return new ResponseEntity<>(b.getExceptionCode(),HttpStatus.BAD_REQUEST);
        }
    }
}
