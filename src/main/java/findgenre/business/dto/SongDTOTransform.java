package findgenre.business.dto;

import findgenre.persistence.entity.Song;

public class SongDTOTransform {
    public SongDTOTransform(){

    }

    public static Song toEntity(SongDTO songDTO){
        Song song = new Song();
        song.setId(songDTO.getId());
        song.setSongId(songDTO.getSongId());
        song.setTitle(songDTO.getTitle());
        song.setGenre(songDTO.getGenre());
        song.setChannelId(songDTO.getChannelId());
        song.setViews(songDTO.getViews());
        song.setLikes(songDTO.getLikes());
        song.setDislikes(songDTO.getDislikes());
        return song;
    }
    public static SongDTO fromEntity(Song song){
        SongDTO songDTO = new SongDTO();
        songDTO.setId(song.getId());
        songDTO.setSongId(song.getSongId());
        songDTO.setTitle(song.getTitle());
        songDTO.setChannelId(song.getChannelId());
        songDTO.setGenre(song.getGenre());
        songDTO.setViews(song.getViews());
        songDTO.setLikes(song.getLikes());
        songDTO.setDislikes(song.getDislikes());
        return songDTO;
    }


}
