package findgenre.business.dto;

import findgenre.persistence.entity.User;

public class UserDTOTransform {

    private UserDTOTransform() {

    }

    public static UserDTO fromEntity(User user){
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setName(user.getName());
        userDTO.setEmail(user.getEmail());
        userDTO.setPassword(user.getPassword());
        return userDTO;
    }

    public static User toRegisterEntity(UserRegisterDTO userRegisterDTO){
        User user = new User();
        user.setName(userRegisterDTO.getName());
        user.setEmail(userRegisterDTO.getEmail());
        user.setPassword(userRegisterDTO.getPassword());
        return user;
    }

    public static UserRegisterDTO fromRegisterEntity(User user){
        UserRegisterDTO userRegisterDTO = new UserRegisterDTO();
        userRegisterDTO.setName(user.getName());
        userRegisterDTO.setEmail(user.getEmail());
        userRegisterDTO.setPassword(user.getPassword());
        return userRegisterDTO;
    }

    public static User toEntity(UserDTO userDTO){
        User user = new User();
        user.setName(userDTO.getName());
        user.setEmail(userDTO.getEmail());
        user.setPassword(userDTO.getPassword());
        return user;
    }


}
