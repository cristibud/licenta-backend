package findgenre.persistence.repository;

import findgenre.persistence.entity.Song;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface SongRepository extends CrudRepository<Song, Integer> {
    List<Song> findAll();
    @Query("select s from Song s where s.songId =:songId")
    Optional<Song> findById(String songId);
    @Query("select s from Song s inner join s.users u where u.id = :userId")
    List<Song> findAllByUserId(int userId);
}
