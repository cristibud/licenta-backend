package findgenre.persistence.repository;

import findgenre.persistence.entity.SongUser;
import org.springframework.data.repository.CrudRepository;

public interface SongUserRepository  extends CrudRepository<SongUser, Integer> {
}
