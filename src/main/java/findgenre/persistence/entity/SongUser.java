package findgenre.persistence.entity;

import javax.persistence.*;

@Entity
@Table(name = "song_user")
public class SongUser {

    @Id
    @GeneratedValue
    private int id;

    @Column(name = "song_id")
    private int songID;

    @Column(name = "user_id")
    private int userID;

    public SongUser() {
    }

    public SongUser(int id, int songID, int userID) {
        this.id = id;
        this.songID = songID;
        this.userID = userID;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSongID() {
        return songID;
    }

    public void setSongID(int songID) {
        this.songID = songID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }
}
