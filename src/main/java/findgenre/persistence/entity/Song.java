package findgenre.persistence.entity;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "song")
public class Song {

    @Id
    @GeneratedValue
    private int id;

    @Column(name = "song_id")
    private String songId;

    @Column(name = "title")
    private String title;

    @Column(name = "channelId")
    private String channelId;

    @Column(name = "genre")
    private String genre;

    @Column(name = "views")
    private Long views;

    @Column(name = "likes")
    private Long likes;

    @Column(name = "dislikes")
    private Long dislikes;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER,cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
    })
    @JoinTable(name = "Song_User",
        joinColumns = {
                @JoinColumn(
                        name = "song_id",
                        referencedColumnName = "id"
                )
        },
        inverseJoinColumns = {
                @JoinColumn(
                        name = "user_id",
                        referencedColumnName = "id"
                )
        }
    )
    private List<User> users = new ArrayList<>();
    public Song(){

    }

    public Song(String songId, String title, String channelId, String genre, Long views, Long likes, Long dislikes) {
        this.songId = songId;
        this.title = title;
        this.channelId = channelId;
        this.genre = genre;
        this.views = views;
        this.likes = likes;
        this.dislikes = dislikes;
    }

    public String getSongId() {
        return songId;
    }

    public void setSongId(String songId) {
        this.songId = songId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public Long getViews() {
        return views;
    }

    public void setViews(Long views) {
        this.views = views;
    }

    public Long getLikes() {
        return likes;
    }

    public void setLikes(Long likes) {
        this.likes = likes;
    }

    public Long getDislikes() {
        return dislikes;
    }

    public void setDislikes(Long dislikes) {
        this.dislikes = dislikes;
    }
}
